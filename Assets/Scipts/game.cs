﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class game : MonoBehaviour
{
	public Text score;
	public static int points =0;
	public Transform spawnPoint;
	public Transform ballPrefab;
	public int mpales;
	public bool start = false;
	public static int combo = 0;
	public bool moreMoves;
	public GameObject panel;
	public bool NoMoreMoves=false;
    private bool ok=false;
	public GameObject moremoves;
	public static bool canClick = false;
	public GameObject obj;
	// Use this for initialization
	void Start ()
	{
		obj= new GameObject();
		score.text="Score :" + points;
		mpales = 60;
		moremoves.SetActive (false);
	}

	public void Moving()
	{
		if(ok==true)
		{
		foreach(var ball in ballScript.balls)
		{
				obj = ball.gameObject;
			if(ball.GetComponent<ballScript>().isMoving)
			{
				return;
					break;
			}
		}
			if (obj.GetComponent<ballScript>().checkForMoves() ==false)
			{
			Camera.main.GetComponent<game>().moremoves.SetActive (true);
			canClick=false;
			}
		}
	}
	
	public void startGame()
	{
	panel.SetActive(false);
		start = true;
	}

	void Update()
	{
		if(ok==false && ballScript.balls.Count==mpales)
		{
			ok=true;
			canClick=true;
		}
		if(start && ballScript.balls.Count<mpales && ok==false)
		{
		makeBalls ();
			canClick=false;
		}
		Moving();
	}

	void makeBalls ()
	{
		Vector3 spawnpos = spawnPoint.position + new Vector3 (UnityEngine.Random.Range (-2f, 2f), 0, 0);
		var bal = Instantiate (ballPrefab, spawnpos, Quaternion.identity) as Transform;
		bal.name = "ball" + Random.Range (0, 999999);
		//numOfBalls++;
		bal.GetComponent<Rigidbody> ().AddForce (new Vector3 (0, -10000, 0));
	}
}
