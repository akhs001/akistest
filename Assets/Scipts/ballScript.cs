﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ballScript : MonoBehaviour
{
	public Color col;
	public int numCol;
	public bool forDelete;
	public static List<GameObject> balls = new List<GameObject>();
	private int combo;
	public int maxBallsToBreak = 3;
	public bool isMoving;
	private Vector3 lastPos = Vector3.zero;

	void Update()
	{
		Vector3 pos= transform.position;
		if(pos == lastPos)
		{
			isMoving=false;
		}
		else
		{
			isMoving=true;
		}
		lastPos=pos;
	}

	public bool checkForMoves()
	{
		foreach(var ball in balls)
		{
			combo=1;
			if(searchNeigbors (ball)>= maxBallsToBreak)
			{
				print ("find " + searchNeigbors (ball) + " color " + ball.GetComponent<ballScript>().col);
				clearBalls();
				return true;
				break;
			}
		}
		clearBalls ();
		return false;
	}
//Ψαχνει τις γειτονικες μπαλες
	 int searchNeigbors(GameObject neighbor)
	{
		Color c = new Color(0,0,0,.5f);
		neighbor.GetComponent<Renderer>().material.color = c;
		foreach(var ball in balls)
		{
			if(ball.gameObject == this.gameObject || ball.GetComponent<Renderer>().material.color.a < .6f) continue;

			ballScript bs = neighbor.GetComponent<ballScript>();
			if((ball.transform.position - neighbor.transform.position).sqrMagnitude <1.5 && ball.GetComponent<ballScript>().numCol == bs.numCol )
			{
				Color col = new Color(0,0,0,.5f);
				ball.GetComponent<Renderer>().material.color = col;
				combo++;
				searchNeigbors (ball);
			}
		}
		return combo;
	}

	void OnDestroy()
	{
		balls.Remove (this.gameObject);
		Destroy (this.gameObject);

	}

	public static void DeleteBalls()
	{
		foreach (var ball in balls.ToList ())
		{
			Color c = ball.GetComponent<Renderer>().material.color;
			if (c.a < .6f)
			{
				Destroy (ball , 2);
				ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
				ball.GetComponent<Rigidbody>().AddForce (new Vector3(0,0,Random.Range (-1000,-1500)));
			}
		}
	}

	public static void clearBalls()
	{
		foreach (var ball in balls.ToList ())
		{
			Color c = ball.GetComponent<Renderer>().material.color;
			if (c.a < .6f)
			{
				ball.GetComponent<Renderer>().material.color=ball.GetComponent<ballScript>().col;
			}
		}
	}

	void OnMouseDown ()
	{  
		if(game.canClick==false) return;
		game.canClick=false;
		combo = 1;
		clearBalls();
		if(searchNeigbors(this.gameObject)>= maxBallsToBreak)
		{
			var tempScore = searchNeigbors (this.gameObject)*7;
			DeleteBalls();
			game.points += tempScore;
			Camera.main.GetComponent<game>().score.text  ="Score :" +  game.points.ToString ();
			game.canClick=true;
		}
		else{
			clearBalls ();
			game.canClick=true;
		}


	}



	void Start ()
	{
		numCol = UnityEngine.Random.Range (1, 4);
		switch (numCol) {
		case 1:
			col = new Color (1, 0, 0);
			transform.tag = "Red";
			break;
		case 2:
			col = new Color (0, 1, 0);
			transform.tag = "Green";
			break;
		case 3:
			col = new Color (0, 0, 1);
			transform.tag = "Blue";
			break;

		}
		GetComponent<Renderer> ().material.color = col;
		balls.Add (this.gameObject);
	}





}
